package it.unifi.micc.java_base;
import java.util.GregorianCalendar;


public class ImmutableClasses {

	public static void main(String[] args) {
		
		
		GregorianCalendar d = new GregorianCalendar();
		Book b1 = new Book("Ubik", 298, d);
		/*Test immutability*/
		System.out.println(b1);
		GregorianCalendar bookPublicationDate = b1.getPublicationDate();
		bookPublicationDate.set(GregorianCalendar.DAY_OF_MONTH, 1);
		bookPublicationDate.set(GregorianCalendar.MONTH, 0);
		bookPublicationDate.set(GregorianCalendar.YEAR, 2016);
		
		System.out.println(b1);
		
		

	}

}
