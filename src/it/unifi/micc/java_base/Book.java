package it.unifi.micc.java_base;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public final class Book {

	//Tutti i campi sono final, sono certo che nessun metodo ne modificherà il valore
	private final String title;
	private final  int numPages;
	private final GregorianCalendar publicationDate;
	
	public Book(String name, int numPages, GregorianCalendar   publicationDate) {
		this.title = name;
		this.numPages = numPages;
		this.publicationDate = publicationDate;
	}
	public String getTitle() {
		return title;
	}
	public int getNumPages() {
		return numPages;
	}
	//non immutable
	public GregorianCalendar getPublicationDate() {
		return publicationDate;
	}
	//defensive copy: immutable
//	public GregorianCalendar getPublicationDate() {
//		return (GregorianCalendar)publicationDate.clone();
//}
	
	public String toString(){
		SimpleDateFormat df = new SimpleDateFormat("d/M/yyyy");
		return title + ", (" + numPages + " pages), " + " published " + df.format(publicationDate.getTime());
	}
	
}
